<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//ruta de prueba 
Route::get('hola' , function(){
    echo "hola 2";
});

//ruta de areglo
Route::get('arreglo',function(){
    //defino un arreglo
    $estudiantes=["AN"=>"ana","MA"=>"maria","VA"=>"valeria","CA"=>"carlos"];
    echo"<pre>";
    //forma var_dump
    //var_dump($estudiantes);
   
    //cicos foreach: recorrer arreglo 
    foreach($estudiantes as $in => $e){
        echo "$e  tiene el indice: $in <br />";

    }
    
    echo"<pre>";
});


//ruta paises 
Route::get('paises',function(){

    $paises=[
        "Colombia"=>[
            "capital"=> "bogota",
            "moneda" => "peso",
            "poblado"=> 50372424,
            "ciudades"=>["Medellin","Cali","Barranquilla"]
        ],
        "peru"=>[
            "capital" => "lima",
            "moneda" => "sol",
            "poblado" => 17517141,
            "ciudades"=>["cuzco","trujillo","arequipa"]
    ],
            
        "Ecuador"=>[
            "capital"=>"quito",
            "moneda" => "dolar",
            "poblado" => 17517141,
            "ciudades"=>["guañaquil","quito","arequipa"]
        ],
        "Brazil"=>[
            "capital" => "Brasilia",
            "moneda" => "dolar",
            "poblado" => 212216052,
            "ciudades"=>["guañaquil","quito","arequipa"]
        ]

    ];

    //Enviar datos de paises a una vista
    //con la fuincion view
    return view('paises')
            ->with("paises", $paises );

});

